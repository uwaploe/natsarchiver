// Archiver subscribes to one or more NATS subjects and stores
// the messages to a series of rotating files.
package main

import (
	"bufio"
	"context"
	"encoding/binary"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"sync"
	"syscall"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/imdario/mergo"

	//stan "github.com/nats-io/go-nats-streaming"
	"github.com/nats-io/stan.go"
)

type dataDesc struct {
	Name     string `toml:"name"`
	Prefix   string `toml:"prefix"`
	Ext      string `toml:"ext"`
	Delim    string `toml:"delim"`
	Linkdir  string `toml:"linkdir"`
	Header   string `toml:"header"`
	InclTime bool   `toml:"incl_time"`
	Sep      string `toml:"sep"`
}

type natsConfig struct {
	Url      string     `toml:"url"`
	Cluster  string     `toml:"cluster"`
	Subjects []dataDesc `toml:"subject"`
}

type sysConfig struct {
	Params map[string]string `toml:"params"`
	Nats   natsConfig        `toml:"nats"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: natsarchiver [options] [configfile]

Subscribes to one or more NATS subjects and stores the messages
to a series of rotating files.
`

var DEFAULT = `
[params]
topdir = "."
interval = "6h"
[nats]
url = "nats://localhost:4222"
cluster = "test-cluster"
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	connectWait = flag.Duration("wait", 5*time.Second,
		"Time to wait for server connection")
)

func newArchiveFile(pathname string) (*os.File, bool, error) {
	log.Printf("Opening %q", pathname)
	// Check if file already exists
	_, err := os.Stat(pathname)
	newfile := os.IsNotExist(err)

	if newfile {
		// Create all directories if needed. We can safely ignore the
		// returned error because it will be caught when we try to
		// open the file.
		os.MkdirAll(filepath.Dir(pathname), 0755)
	}

	f, err := os.OpenFile(pathname, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	return f, newfile, err
}

// Archiver accepts byte slices from a channel and appends them to a
// file. Returns when the Context is cancelled.
func archiver(ctx context.Context, sc stan.Conn, pathname string, dd dataDesc,
	lastSeq uint64) uint64 {
	var seq uint64

	log.Printf("archiver routine starting (%s)", pathname)
	f, isNew, err := newArchiveFile(pathname)
	if err != nil {
		log.Printf("Cannot open file %s: %v", pathname, err)
		return seq
	}

	// On closing, optionally link the file into "linkdir"
	defer func(d string) {
		err := f.Close()
		if err != nil {
			log.Printf("Closing %s failed: %v", pathname, err)
			return
		}
		// Remove the file if empty
		info, err := os.Stat(pathname)
		if err == nil && info.Size() == 0 {
			os.Remove(pathname)
			return
		}
		if d != "" {
			os.MkdirAll(d, 0755)
			os.Link(pathname,
				filepath.Join(d, filepath.Base(pathname)))
		}
	}(dd.Linkdir)

	wbuf := bufio.NewWriter(f)
	defer wbuf.Flush()
	eol := []byte(dd.Delim)

	var mcb stan.MsgHandler

	// Message callback function
	if dd.Ext == "lcm" {
		ev_index := uint64(0)
		// For LCM data, ensure that the Event number starts at zero and
		// increases montonically
		mcb = func(m *stan.Msg) {
			binary.BigEndian.PutUint64(m.Data[4:], ev_index)
			ev_index++
			wbuf.Write(m.Data)
			wbuf.Write(eol)
			err = wbuf.Flush()
			if err != nil {
				log.Printf("Write error on %s: %v", pathname, err)
			} else {
				seq = m.Sequence
			}
		}
	} else {
		mcb = func(m *stan.Msg) {
			if dd.Header != "" && isNew {
				wbuf.Write([]byte(dd.Header))
				wbuf.Write(eol)
				isNew = false
			}
			if dd.InclTime && dd.Sep != "" {
				fmt.Fprintf(wbuf, "%d%s", m.Timestamp, dd.Sep)
			}
			wbuf.Write(m.Data)
			wbuf.Write(eol)
			err = wbuf.Flush()
			if err != nil {
				log.Printf("Write error on %s: %v", pathname, err)
			} else {
				seq = m.Sequence
			}
		}
	}

	var sub stan.Subscription
	sub, err = sc.Subscribe(dd.Name, mcb, stan.DurableName(dd.Name+"-archiver"))
	if err != nil {
		log.Printf("Cannot subscribe to %q: %v", dd.Name, err)
		return 0
	}

	<-ctx.Done()
	sub.Unsubscribe()
	log.Printf("archiver routine done (%s)", pathname)

	return seq + 1
}

func stanConnect(url, cluster string, tries int) (stan.Conn, error) {
	var (
		sc  stan.Conn
		err error
	)

	delay := int64(100)
	for i := 0; i < tries; i++ {
		sc, err = stan.Connect(cluster, "archiver", stan.NatsURL(url),
			stan.ConnectWait(*connectWait),
			stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
				log.Fatalf("Connection lost, reason: %v", reason)
			}))
		if err != nil {
			log.Printf("Connection attempt %d failed: %v", i+1, err)
			time.Sleep(time.Duration(delay) * time.Millisecond)
			delay = delay * 2
			continue
		}
		return sc, nil
	}

	return sc, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err         error
		defcfg, cfg sysConfig
		interval    time.Duration
		topdir      string
		ok          bool
	)

	if err = toml.Unmarshal([]byte(DEFAULT), &defcfg); err != nil {
		log.Fatalf("Cannot parse default configuration: %v", err)
	}

	args := flag.Args()

	if len(args) > 0 {
		log.Println("Reading configuration file")
		b, err := ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatalf("Cannot read config file: %v", err)
		}

		if err = toml.Unmarshal(b, &cfg); err != nil {
			log.Fatalf("Cannot parse configuration file: %v", err)
		}
	}

	mergo.Merge(&cfg, defcfg)

	if len(cfg.Nats.Subjects) == 0 {
		log.Print("No subjects to monitor, exiting ...")
		os.Exit(0)
	}

	if val, ok := cfg.Params["interval"]; ok {
		interval, err = time.ParseDuration(val)
		if err != nil {
			log.Fatalf("Cannot parse interval: %v", err)
		}
	} else {
		log.Fatal("Roll-over interval not specified!")
	}

	if topdir, ok = cfg.Params["topdir"]; !ok {
		log.Fatal("Archive directory not specified!")
	}

	// Create functions to implement the archive file naming scheme.
	var archive_name func(dataDesc) string
	if interval >= (time.Hour * 24) {
		archive_name = func(dd dataDesc) string {
			t := time.Now().UTC()
			if dd.Delim == "" {
				dd.Delim = "dat"
			}
			return fmt.Sprintf("%s/%d/%03d/%s-%s.%s",
				topdir,
				t.Year(),
				t.YearDay(),
				dd.Prefix,
				t.Format("20060102"), dd.Ext)
		}
	} else if interval >= time.Hour {
		archive_name = func(dd dataDesc) string {
			t := time.Now().UTC()
			if dd.Delim == "" {
				dd.Delim = "dat"
			}
			return fmt.Sprintf("%s/%d/%03d/%s-%s.%s",
				topdir,
				t.Year(),
				t.YearDay(),
				dd.Prefix,
				t.Format("20060102-15"), dd.Ext)
		}
	} else {
		archive_name = func(dd dataDesc) string {
			t := time.Now().UTC()
			if dd.Delim == "" {
				dd.Delim = "dat"
			}
			return fmt.Sprintf("%s/%d/%03d/%s-%s.%s",
				topdir,
				t.Year(),
				t.YearDay(),
				dd.Prefix,
				t.Format("2006010215-04"), dd.Ext)
		}
	}

	var (
		ctx, ar_ctx context.Context
		cancel      context.CancelFunc
		wg          sync.WaitGroup
	)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Top level context
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel()

	sc, err := stanConnect(cfg.Nats.Url, cfg.Nats.Cluster, 5)
	if err != nil {
		log.Fatalf("Cannot connect to NATS server: %v", err)
	}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	log.Printf("NATS Archiver starting: %s", Version)
	deadline := time.Now().Truncate(interval).Add(interval)
	log.Printf("Next deadline: %s", deadline)

	// Track the message sequence number for each subject
	seqNums := make(map[string]uint64)

	var maplock = &sync.Mutex{}
	// Start archiver goroutines to end at the next deadline.
	ar_ctx, _ = context.WithDeadline(ctx, deadline)
	for _, dd := range cfg.Nats.Subjects {
		wg.Add(1)
		go func(cfg dataDesc) {
			defer wg.Done()
			n := seqNums[cfg.Name]
			n = archiver(ar_ctx, sc, archive_name(cfg), cfg, n)
			maplock.Lock()
			seqNums[cfg.Name] = n
			maplock.Unlock()
		}(dd)
	}

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case <-ar_ctx.Done():
			if ar_ctx.Err() == context.DeadlineExceeded {
				// Deadline expired, set the next deadline, wait for archivers
				// to exit and start the next group
				deadline = time.Now().Truncate(interval).Add(interval)
				log.Println("Waiting for tasks to finish ...")
				wg.Wait()
				log.Printf("Next deadline: %v", deadline)
				ar_ctx, _ = context.WithDeadline(ctx, deadline)
				for _, dd := range cfg.Nats.Subjects {
					wg.Add(1)
					go func(cfg dataDesc) {
						defer wg.Done()
						n := seqNums[cfg.Name]
						n = archiver(ar_ctx, sc, archive_name(cfg), cfg, n)
						maplock.Lock()
						seqNums[cfg.Name] = n
						maplock.Unlock()
					}(dd)
				}

			} else {
				// Cancelled
				break loop
			}
		}
	}

	cancel()
	log.Println("Waiting for tasks to finish ...")
	wg.Wait()
}
