module bitbucket.org/uwaploe/natsarchiver

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.0.0-20150518234257-fa3f63826f7c // indirect
	github.com/hashicorp/raft v1.0.0 // indirect
	github.com/imdario/mergo v0.3.6
	github.com/lib/pq v1.0.0 // indirect
	github.com/nats-io/gnatsd v1.3.0 // indirect
	github.com/nats-io/go-nats v1.7.0 // indirect
	github.com/nats-io/go-nats-streaming v0.4.0 // indirect
	github.com/nats-io/nats-server/v2 v2.1.8 // indirect
	github.com/nats-io/nats-streaming-server v0.11.2 // indirect
	github.com/nats-io/stan.go v0.7.0
	github.com/pascaldekloe/goe v0.0.0-20180627143212-57f6aae5913c // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/vmihailenco/msgpack.v2 v2.9.1 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
	labix.org/v2/mgo v0.0.0-20140701140051-000000000287 // indirect
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
)

go 1.13
