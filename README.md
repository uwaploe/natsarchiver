# NATS Message Archiver

Natsarchiver is a tool to archive the contents of messages published to a [NATS Streaming Server](https://nats.io/documentation/streaming/nats-streaming-intro/). The messages are stored in a series of rotating files.

## Configuration File

A [TOML](https://github.com/toml-lang/toml) file is used for configuration, below is a simple example.

``` toml
[params]
topdir = "/data"
interval = "6h"
[nats]
url = "nats://localhost:4222"
cluster = "test-cluster"
# There is one nats.subject entry for each archive file
  [[nats.subject]]
  # Subject name
  name = "data.test"
  # Archive file prefix
  prefix = "test"
  # Message delimiter (none)
  delim = ""
  # Archive file extension
  ext = "bin"
  [[nats.subject]]
  name = "data.test2"
  prefix = "test2"
  delim = "\n"
  ext = "json"
```
